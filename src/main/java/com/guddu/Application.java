package com.guddu;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.guddu.model.Account;
import com.guddu.model.Database;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
    	Database.initBank();
        SpringApplication.run(Application.class, args);
    }

}