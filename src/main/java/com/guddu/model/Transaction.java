package com.guddu.model;

public class Transaction {
	private int _id;
	private String _date;
	private int amount;
	private TransactionType _transactionType;
	private ExtraInfo extraInfo;
	public Transaction() {
		super();
	}
	public Transaction(ExtraInfo extraInfo) {
		//super();
		this.setExtraInfo(extraInfo);
	}
	public Transaction(int amount) {
		super();
		this.setAmount(amount);
	}
	public Transaction(int amount, ExtraInfo extraInfo) {
		super();
		this.setAmount(amount);
		this.setExtraInfo(extraInfo);
	}
	public Transaction(int id, int amount, TransactionType transactionType, ExtraInfo extraInfo) {
		super();
		_id = id;
		this.setAmount(amount);
		_transactionType = transactionType;
		this.setExtraInfo(extraInfo);
	}
	
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public ExtraInfo getExtraInfo() {
		return extraInfo;
	}
	
	public void setExtraInfo(ExtraInfo extraInfo) {
		this.extraInfo = extraInfo;
	}
	
}