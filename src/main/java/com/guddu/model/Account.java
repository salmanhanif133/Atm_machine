package com.guddu.model;

import java.util.ArrayList;

public class Account {
	public int accountId;
	public String accountName;
	public int balance;
	public int pin;
	private ArrayList<Transaction> _transactions;
	public Account(int accountId, String accountName, int balance, int pin) {
		this._transactions = new ArrayList<Transaction>();
		this.accountId = accountId;
		this.accountName = accountName;
		this.balance = balance;
		this.pin = pin;
	}
	public int getPin() {
		return this.pin;
	}
	public void addTransaction(Transaction t) {
		this._transactions.add(t);
		
	}
	public int getNumberOfTransactions() {
		return _transactions.size()+1;
	}
	
}
