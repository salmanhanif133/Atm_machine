package com.guddu.model;

public class Login {
	private int pin;
	private Account account;
	private String status="Successful";
	
	public Login() {
		super();
		this.pin =0;	
	}
	
	public Login(int pin) {
		super();
		this.pin =pin;	
	}

	public int getPin() {
		return pin;
	}
	
	public void setPin(int pin) {
		this.pin = pin;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account  = account;
	}
	
}