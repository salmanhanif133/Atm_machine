package com.guddu.model;

public enum TransactionType {
	CASH_DEPOSIT, CASH_WITHDRAWAL, INTER_ACCOUNT, MINI_STATEMENT
}	