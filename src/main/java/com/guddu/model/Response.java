package com.guddu.model;

public class Response {
	public Object body;
	public String message;
	public int current_balance;
	static String balanceIntargetAccountId;
	static String balanceInaccountId;
	private int transactions;
	public Response(String message) {
		this.message = message;
	}
	public Response(String message, Object body, int current_balance) {
		this.body = body;
		this.message = message;
		this.current_balance = current_balance;
	}
	public Response(String message, Object body, int transactions, int current_balance) {
		this.message = message;
		this.body=body;
		this.transactions = transactions;
		this.current_balance = current_balance;
	}
	public Response(String message, Object body, String balanceIntargetAccountId, String balanceInaccountId) {
		this.body = body;
		this.message = message;	
		Response.balanceIntargetAccountId =balanceIntargetAccountId;
		Response.balanceInaccountId = balanceInaccountId;
	}
	
	public void setTransactions(int transactions) {
		this.transactions = transactions;
	}
	public int getTransactions() {
		return this.transactions;
	}
}
