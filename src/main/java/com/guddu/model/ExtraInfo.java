package com.guddu.model;

public class ExtraInfo {
	public int targetAccountId;
	public int accountId;
	public String type;
	public ExtraInfo() {
		super();
	}
	public ExtraInfo(int accountId, String type) {
		super();
		this.accountId = accountId;
		this.type = type;
	}
	public ExtraInfo(int targetAccountId, int accountId, String type) {
		super();
		this.targetAccountId = targetAccountId;
		this.accountId = accountId;
		this.type = type;
	}
}
