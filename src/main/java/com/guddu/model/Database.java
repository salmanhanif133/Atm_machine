package com.guddu.model;
import java.util.*;
import java.io.*;

public class Database {
	public static ArrayList<Account> account = new ArrayList<>();
	private static ArrayList<Transaction> _transactions;
	private static int ACCOUNT_ID;
	private static int T_ID;
	public static void initBank() {
		Database._transactions = new ArrayList<Transaction>();
		Database.ACCOUNT_ID = 0;
		Database.T_ID = 0;
		// The name of the file to open.
        String fileName = "E:\\IBA\\2nd Sems\\ITS\\spring-guddu-helloworld\\src\\main\\java\\com\\guddu\\model\\temp.txt";

        // This will reference one line at a time
        String line = null;
        int i=1;
        String acc="";
        String amount="";
        String pin="";
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
            	switch (i) {
				case 1:
					acc=line;
					i++;break;
				case 2:
					amount=line;
					i++;break;
				case 3:
					pin=line;
					i++;
					break;
				}
               if(i==4) {
            	 Database.addAccount(acc, Integer.parseInt(amount), Integer.parseInt(pin));
               i=1;
               }
            	
            	
            }   

            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
            
        }
		//Hard code add account
			//Database.addAccount("Salman", 500, 1234);
			//Database.addAccount("Mustafa", 20000, 1235);
			//Database.addAccount("Murtaza", 20000, 1236);
	}
	public static void addAccount(String accountName, int balance, int pin) {
		Database.account.add(new Account(Database.ACCOUNT_ID++, accountName, balance, pin));
	}
	public static Response performTransaction(Transaction transaction) {
		switch (transaction.getExtraInfo().type) {
			case "cash_withdraw": 
				return withdrawMoney(transaction);
			case "cash_deposit": 
				return depositMoney(transaction);
			case "inter_account": 
				return interAccountTransfer(transaction);
			case "mini_statement":
				return miniStatement(transaction);
		}
		
		return null;
	}
	public static Response withdrawMoney(Transaction transaction) {
		try {
			Transaction t;
			Account a = Database.getAccount(transaction.getExtraInfo().accountId);
			t = new Transaction(T_ID++, transaction.getAmount(), TransactionType.CASH_WITHDRAWAL, transaction.getExtraInfo());

			String success = "Transaction Successful";
			if((a.balance-transaction.getAmount())<0) {
				success = "Insufficent funds!";
			} else if(transaction.getAmount()<0) {
				success = "Invalid Input!";
			}else {
				a.addTransaction(t);
				Database._transactions.add(t);
				a.balance -= transaction.getAmount();				
			}
			Response r = new Response(success, t, a.balance);
			return r;
		}catch(Exception e) {
			Response r = new Response("Cannot perform operation", e, 0);
			return r;
		}
		
	}
	public static Response depositMoney(Transaction transaction) {
		try {
			Transaction t;
			Account a = Database.getAccount(transaction.getExtraInfo().accountId);
			t = new Transaction(T_ID++, transaction.getAmount(), TransactionType.CASH_DEPOSIT, transaction.getExtraInfo());

			String success = "Transaction Successful!";
			if(transaction.getAmount()<0) {
				success = "Invalid Input!";
			} else {
				a.addTransaction(t);
				Database._transactions.add(t);
				a.balance += transaction.getAmount();				
			}
						
			Response r = new Response(success, t, a.balance);
			return r;
		}catch(Exception e) {
			Response r = new Response("Cannot perform operation", e, 0);
			return r;
		}
	}
	public static Response interAccountTransfer(Transaction transaction) {
		try {
			Transaction t;
			Account a = Database.getAccount(transaction.getExtraInfo().accountId);
			Account b = Database.getAccount(transaction.getExtraInfo().targetAccountId);
			t = new Transaction(T_ID++, transaction.getAmount(), TransactionType.INTER_ACCOUNT, transaction.getExtraInfo()/*, transaction.secondgetExtraInfo()*/);
			a.addTransaction(t);
			b.addTransaction(t);
			Database._transactions.add(t);
			a.balance -= transaction.getAmount();
			b.balance += transaction.getAmount();
			Response r = new Response("Transaction Successful", t, "New balance in #"+Integer.toString(b.accountId)+": "+ Integer.toString(b.balance), "Remaining balance in #"+Integer.toString(a.accountId)+": "+ Integer.toString(a.balance));
			//Response r = new Response("Transaction Successful", t, String.valueOf(b.balance), String.valueOf(a.balance));
			return r;
		}catch(Exception e) {
			Response r = new Response("Cannot perform operation", e, 0);
			return r;
		}
	}
	public static Response miniStatement(Transaction transaction) {
		Transaction t;
		t = new Transaction(T_ID++, transaction.getAmount(), TransactionType.MINI_STATEMENT, transaction.getExtraInfo()/*, transaction.secondgetExtraInfo()*/);
		Account a = Database.getAccount(transaction.getExtraInfo().accountId);
		a.addTransaction(t);
		Database._transactions.add(t);
		Response r = new Response("Transactionasdsa Successfull", transaction, a.getNumberOfTransactions(),	 a.balance);
		return r;
	}
	public static Account getAccount(int accountId) {
		for (int i = 0; i < Database.account.size(); i++) {
			if (Database.account.get(i).accountId == accountId) {
				return Database.account.get(i);
			}
		}
		return null;
	}
	/*public static Account getSecondAccount(int accountId) {
		for (int i = 0; i < Database.account.size(); i++) {
			if (Database.account.get(i).accountId == accountId) {
				return Database.account.get(i);
			}
		}
		return null;
	}*/
}
