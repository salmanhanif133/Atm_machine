package com.guddu.api;

import java.util.*;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.guddu.model.Point;
import com.guddu.model.Response;
import com.guddu.model.Login;
import com.guddu.model.Account;
import com.guddu.model.Database;
import com.guddu.model.Transaction;
import com.guddu.model.ExtraInfo;
@RestController

@RequestMapping("/api")



class APIController {
	
	

    @GetMapping("/login")
    public Login login() {
    	
    	Login a = new Login(123);
    	return a;
    }
    
    @CrossOrigin(origins = "http://localhost:10001")
    @PostMapping("/login")
    public Login login(@RequestBody Login login) {

    	for(int i=0; i<Database.account.size(); i++) {
    		if(login.getPin()==Database.account.get(i).getPin()) {
    			login.setStatus("Successfull");
    			login.setAccount(Database.account.get(i));
    			break;    			
    		} else {
    			login.setStatus("Incorrect Pin");
    		}
    		
    		
    	}
    	return login;
    	
    	
    }
    @CrossOrigin(origins = "http://localhost:10001")
    @PostMapping("/transaction")
    public Response transactionPost(@RequestBody Transaction transaction) {
    	return Database.performTransaction(transaction);
    }
    @CrossOrigin(origins = "http://localhost:10001")
    @PostMapping("/type")
    public ExtraInfo extra(@RequestBody ExtraInfo extra) {
    	return extra;
    }
    
    @GetMapping("/hello")
    public String hello() {
        return "Hello, World!";
    }

    @GetMapping("/echo/{name}")
    public String echo(@PathVariable("name") String name) {
        return name;
    }

   // @GetMapping("/point")
   // public Point point() {
  //  	Point abc = new Point(3,4);
  //      return abc;
  //  }
    
  //  @CrossOrigin(origins = "http://localhost:10001")
  //  @PostMapping("/point")
  //  public Point post(@RequestBody Point point) {
   // 	return point;
    	
 //   }
    
}
